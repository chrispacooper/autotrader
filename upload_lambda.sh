set -e

echo "Cleanup Previous"
rm -f function.zip

echo "Zipping Packages"
cd venv/lib/python3.6/site-packages/
zip -r9 ../../../../function.zip .
cd ../../../../

zip -g function.zip save_html_to_s3.py autotrader_s3.py

echo "Updating funciton"
aws lambda update-function-code --function-name save_html_to_s3 --zip-file fileb://function.zip

echo "Updating function configuration"
aws lambda update-function-configuration --function-name save_html_to_s3 --handler save_html_to_s3.save_html --timeout 60 --runtime python3.6
