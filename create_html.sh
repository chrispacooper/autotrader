set -e

echo "Downloading data"
python -c "import autotrader_s3; autotrader_s3.bucket().download_file('search_count/out.csv','data/ev_count.csv')"

echo "Creating html"
Rscript -e "rmarkdown::render('second_hand_ev.Rmd')"

echo "Uploading html"
# Content type needs to be set, otherwise visiting the page downloads the file
# instead of displaying it.
python -c "import website_s3; website_s3.bucket().upload_file('second_hand_ev.html','index.html', ExtraArgs={'ContentType': 'text/html'})"
