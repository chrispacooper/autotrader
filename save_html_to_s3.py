import random
import time
from datetime import datetime, date
import requests

import autotrader_s3

def save_html(event, context):

    urls = ['https://www.autotrader.co.uk/car-search?search-target=usedcars&postcode=SE100RR&radius=&make=&price-search-type=total-price&price-from=&price-to=30000&year-from=2017&maximum-mileage=&zero-to-60=&quantity-of-doors=&minimum-seats=&annual-tax-cars=&colour=&keywords=&fuel-type=Electric&maximum-badge-engine-size=&transmission=&co2-emissions-cars=&maximum-seats=&insuranceGroup=&seller-type=',
            'https://www.autotrader.co.uk/car-search?search-target=usedcars&postcode=SE100RR&radius=&make=&price-search-type=total-price&price-from=&price-to=30000&year-from=2016&maximum-mileage=&zero-to-60=&quantity-of-doors=&minimum-seats=&annual-tax-cars=&colour=&keywords=&fuel-type=Electric&maximum-badge-engine-size=&transmission=&co2-emissions-cars=&maximum-seats=&insuranceGroup=&seller-type=',
            'https://www.autotrader.co.uk/car-search?search-target=usedcars&postcode=SE100RR&radius=&make=&price-search-type=total-price&price-from=&price-to=30000&year-from=2018&maximum-mileage=&zero-to-60=&quantity-of-doors=&minimum-seats=&annual-tax-cars=&colour=&keywords=&fuel-type=Electric&maximum-badge-engine-size=&transmission=&co2-emissions-cars=&maximum-seats=&insuranceGroup=&seller-type=',
            'https://www.autotrader.co.uk/car-search?search-target=usedcars&postcode=SE100RR&radius=&make=&price-search-type=total-price&price-from=&price-to=30000&year-from=2019&maximum-mileage=&zero-to-60=&quantity-of-doors=&minimum-seats=&annual-tax-cars=&colour=&keywords=&fuel-type=Electric&maximum-badge-engine-size=&transmission=&co2-emissions-cars=&maximum-seats=&insuranceGroup=&seller-type=',
            ]

    for url in urls:
        download_html(url)
        # Must be at least one second because
        # files are named based on download time
        time.sleep(random.uniform(1,5))

def download_html(url):
    print("Downloading {}".format(url))
    r = requests.get(url)

    file_name = datetime.today().strftime('%Y%m%d%H%M%S')

    autotrader_s3.bucket().put_object(Key=file_name, Body=r.text)
