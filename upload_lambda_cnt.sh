set -e

echo "Cleanup Previous"
rm -f function.zip

echo "Zipping Packages"
cd venv/lib/python3.6/site-packages/
zip -r9 ../../../../function.zip .
cd ../../../../

echo "Zipping files"
zip -g function.zip save_html_to_s3.py autotrader_s3.py extract_count_from_html.py

#echo "Uploading to aws"
#aws lambda create-function --function-name transform_to_csv --runtime python3.6 --handler extract_count_from_html.html_to_csv --role arn:aws:iam::286734493493:role/lambda-s3 --zip-file fileb://function.zip --timeout 120

echo "Updating funciton"
aws lambda update-function-code --function-name transform_to_csv --zip-file fileb://function.zip

echo "Updating function configuration"
aws lambda update-function-configuration --function-name transform_to_csv --handler extract_count_from_html.html_to_csv --timeout 240 --runtime python3.6
