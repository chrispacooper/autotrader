from lxml import html
import re
import os
import autotrader_s3
import csv


def cnt_results(file_path):
    """Get the number of results from one of the html files"""
    print("Counting results")
    tree = html.parse(file_path)

    # Extract the text containing the number of results
    x_selector = '//h1[contains(@class, "search-form__count js-results-count")]/text()'
    text = tree.xpath(x_selector)
    assert len(text) == 1

    # Extract the number of results
    number = re.findall('^([,0-9]+) cars found', text[0])
    assert len(number) == 1

    digits_only = number[0].replace(',','')

    return(digits_only)


def runtime_from_file(file_path):

    runtime = os.path.basename(file_path)

    return(runtime)


def get_year_from(file_path):

    tree = html.parse(file_path)

    # Extract the text containing the number of results
    x_selector = '//a[contains(@class, "atc-primary-nav__top-level atc-primary-nav__sign-in tracking-header-link")]'
    href = [x.attrib['href'] for x in tree.xpath(x_selector)]
    assert len(href) == 1

    # Extract the number of results
    year = re.findall('year-from=([0-9]{4})', href[0])
    assert len(year) == 1

    return(year[0])


def parse_file(file_path):

    print("Parsing file {}".format(file_path))
    cnt = cnt_results(file_path)
    runtime = runtime_from_file(file_path)
    year_from = get_year_from(file_path)

    return(cnt, runtime, year_from)


def download_s3_objects(data_folder):
    bucket = autotrader_s3.bucket()

    # All file names at top level (no folders)
    object_paths = bucket.objects.all()
    file_names = [x.key for x in object_paths if "/" not in x.key]

    for f in file_names:
        bucket.download_file(f, os.path.join(data_folder, f))


def parse_local_files(data_folder):

    res = []
    with os.scandir(data_folder) as list_entries:
        for entry in list_entries:
            if not entry.name.startswith('.') and entry.is_file():
                cnt, runtime, year_from = parse_file(entry.path)
                res.append({'cnt': cnt, 'runtime': runtime, 'year_from': year_from})

    return(res)


def save_results(res, file_path):
    keys = res[0].keys()
    with open(file_path, 'w+') as o:
        dw = csv.DictWriter(o, keys)
        dw.writeheader()
        dw.writerows(res)


def upload_to_s3(file_path):
    bucket = autotrader_s3.bucket()
    bucket.upload_file(file_path, 'search_count/out.csv')


def html_to_csv(event, context):

    # These really should not be hardcoded.
    # However it is worth noting that I'm using /tmp when on
    # AWS lambda. This is the only folder that lambda functions
    # can access, so watch out for that.

    data_folder = './data/downloads'
    out_file = './data/out.csv'
    download_s3_objects(data_folder)
    res = parse_local_files(data_folder)
    save_results(res, out_file)
    upload_to_s3(out_file)

if __name__ == '__main__':
    html_to_csv(1, 2)
