# Autotrader

Project for trying out aws lambda as much as anything. Also ended up seeing how to host a website on s3, and misusing gitlab ci as a scheduler.

Nasty stuff all over the place, hardcoded file paths and more, enjoy :)

## Structure:

1) save_html_to_s3.py
Module that contains the code to download the html of a couple of autotrader searches to an s3 bucket. The save_html function is the entry point.

2) extract_count_from_html.py
Module that takes the files from s3 and extracts the interesting information into a flat file. The html_to_csv function is the entry point.

3) create_html.sh
Script to take the flat file of interesting information and create the website containing graphs based on that information.

## Deployment

1) upload_lambda.sh deploys save_html_to_s3 as a lambda function. I've scheduled it manually using the AWS web interface for now.

2) upload_lambda_cnt.sh deploys extract_count_from_html as a lambda function. I've scheduled it manually using the AWS web interface for now.

3) gitlab runs create_html.sh using gitlab's ci feature. Again, that is manually scheduled on gitlab's web interfact so far.

## Background setup:

### AWS Lambda
For uploading to AWS from my local setup I have a ~/.aws/credentials file with an access and secret access key. The region is also set, either in that file or ~/.aws/config. The keys are linked to an IAM account with the following policies:
* AWSLambdaFullAccess
* AmazonS3FullAccess

For the lambda function to run it has been assigned a role with
* AmazonS3FullAccess
* AWSLambdaBasicExecutionRole

It has been scheduled using CloudWatch events. Would like to look in to doing that programmatically.

Alerts. I set up cloudfront to alert me any time a lambda function runs, or if none run within a day. 

### Gitlab/EC2
See .gitlab-ci.yml for project specific config. 

Had to configure an EC2 instance to be a Gitlab remote. 

Set up an EC2 instance to run create_html.sh. Really messy. Had to redo soft links for R and pandoc, installing was a pain. Permissions were neat though.

## Resources:

Deploying with python packages
https://docs.aws.amazon.com/lambda/latest/dg/lambda-python-how-to-create-deployment-package.html#python-package-venv

Uploading files to s3:
https://docs.aws.amazon.com/code-samples/latest/catalog/python-s3-s3-python-example-upload-file.py.html

Using s3 for static websites:
https://medium.com/@kyle.galbraith/how-to-host-a-website-on-s3-without-getting-lost-in-the-sea-e2b82aa6cd38
https://stackoverflow.com/questions/30163435/why-does-website-hosted-by-amazon-s3-make-me-download-all-pages-except-index-htm

Gitlab CI:
Trying to set up gitlab to run jobs for me like jenkins did. Really it's intended for CI. Setting up an ec2 instance as the runner.

* Install on ec2 instance: https://docs.gitlab.com/runner/install/linux-repository.html

* Register it: https://docs.gitlab.com/runner/register/index.html
** Page for runner access token: https://gitlab.com/chrispacooper/autotrader/-/settings/ci_cd

By default runs whenever code is pushed to repo, uses file in repo to determine what to run, see Projects/autotrader for a bash example. Can schedule too.
